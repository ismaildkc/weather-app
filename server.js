const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const app = express()

const apiKey = 'ee12591ae1eb8c416dee37e4501b7c2e';

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs')

app.get('/', function(req, res){
    res.send('<a href="/istanbul">İstanbul</a> <br> <a href="/berlin">Berlin</a> <br> <a href="/london">London</a> <br> <a href="/madrid">Madrid</a> <br> <a href="/rome">Rome</a>');
});

app.get('/:cities?', function(req, res){

    let city = req.params.cities;;
    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`

    request(url, function (err, response, body) {
        let weather = JSON.parse(body)
        let weatherText = `${weather.name} : ${weather.main.temp} Degrees and Wind speed: ${weather.wind.speed}`;
        res.render('index', {weather: weatherText, error: null});
    });

})



app.listen(3000, function () {
  console.log('listen on: 3000');
})